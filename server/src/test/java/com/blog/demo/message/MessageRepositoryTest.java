package com.blog.demo.message;

import com.blog.demo.user.User;
import com.blog.demo.user.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class MessageRepositoryTest {

    @Autowired
    private MessageRepository underTest;
//    @Autowired
    @Autowired
    private UserRepository userRepository;
//    MessageRepositoryTest(MessageRepository underTest) {
//        this.underTest = underTest;
//    }
    private User user0;
    private User user1 ;

    private Message message0;
    private Message message1;


    @BeforeEach
    void init() {

        this.user0 = new User(
                "user0@gmail.com",
                "password0",
                true
        );
        this.user1 = new User(
                "user1@gmail.com",
                "password1",
                true
        );
        this.userRepository.save(user0);
        this.userRepository.save(user1);

        this.message0 = new Message();
        this.message0.setContent("Hello");
        this.message0.setUserIdFrom(this.user0.getId());
        this.message0.setUserIdTo(this.user1.getId());
        LocalDateTime created =  LocalDateTime.now();
        this.message0.setCreated(created);
        this.underTest.save(this.message0);

        this.message1 = new Message();
        this.message1.setContent("Hello1");
        this.message1.setUserIdFrom(this.user1.getId());
        this.message1.setUserIdTo(this.user0.getId());
        this.message1.setCreated(LocalDateTime.now());
        this.underTest.save(this.message1);
        this.user0.setMessagesFrom(new ArrayList<>());

        this.user0.setMessagesTo(new ArrayList<>());
        this.user1.setMessagesFrom(new ArrayList<>());
        this.user1.setMessagesTo(new ArrayList<>());

        this.user0.addMessageFrom(this.message0);
        this.user1.addMessageTo(this.message0);
        this.user0.addMessageTo(this.message1);
        this.user1.addMessageFrom(this.message1);

    }
    @Test

    void findMessagesByEmail() {

            assertEquals(2,underTest.findMessagesByEmail("user0@gmail.com").size());
            assertEquals(2,underTest.findMessagesByEmail("user1@gmail.com").size());
    }

    @Test
    void findMessagesByUserId() {
        assertEquals(2,underTest.findMessagesByUserId(user0.getId()).size());
        assertEquals(2,underTest.findMessagesByUserId(user1.getId()).size());
    }

    @Test
    void findFriends() {
        List<User> friends = underTest.findFriends(0L);
        assertEquals(1, friends.size());
    }
}