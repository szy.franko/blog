package com.blog.demo.helpers;

import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
public class Image {

    public static void scaleAndSaveImage(MultipartFile file, int targetWidth, int targetHeight, String outputPath) throws IOException {
        // Wczytaj obraz z MultipartFile
        BufferedImage inputImage = ImageIO.read(file.getInputStream());

        // Oblicz proporcje skalowania
        double scaleX = (double) targetWidth / inputImage.getWidth();
        double scaleY = (double) targetHeight / inputImage.getHeight();

        // Wybierz mniejszą wartość proporcji skalowania, aby zachować proporcje obrazka
        double scale = Math.min(scaleX, scaleY);

        // Oblicz nowe wymiary obrazka
        int newWidth = (int) (inputImage.getWidth() * scale);
        int newHeight = (int) (inputImage.getHeight() * scale);

        // Utwórz nowy obrazek o przeskalowanych wymiarach
        BufferedImage outputImage = new BufferedImage(newWidth, newHeight, inputImage.getType());

        // Uzyskaj obiekt Graphics2D, aby móc przeskalować obrazek
        Graphics2D g2d = outputImage.createGraphics();

        // Przeskaluj obrazek proporcjonalnie
        g2d.drawImage(inputImage, 0, 0, newWidth, newHeight, null);
        g2d.dispose();

        // Zapisz przeskalowany obrazek do pliku na określonej ścieżce
        ImageIO.write(outputImage, "jpg", new File(outputPath));
    }

}
