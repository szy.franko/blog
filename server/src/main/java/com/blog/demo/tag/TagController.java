package com.blog.demo.tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(
        origins = {
                "http://localhost:5173"
        },
        methods = {
                RequestMethod.OPTIONS,
                RequestMethod.GET,
                RequestMethod.PUT,
                RequestMethod.DELETE,
                RequestMethod.POST
        })
@RestController
public class TagController {

    private final TagService tagService;
    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }
    @PostMapping(path="/api/v1/tag")
    Tag addTag(@RequestBody Map<String,String> body) {
        return this.tagService.addTag(body);
    }

    @GetMapping(path="/api/v1/tag")
    List<Tag> getTags(@RequestParam String prefix) {
        return this.tagService.getTags(prefix);
    }
}
