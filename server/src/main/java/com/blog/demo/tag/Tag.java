package com.blog.demo.tag;

import com.blog.demo.post.Post;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Tag {
    @Id
    @SequenceGenerator(
            name="tag_sequence",
            sequenceName = "tag_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "tag_sequence"
    )
    private Long id;
    private String content;



    public Tag(Long id, String content) {
        this.id = id;
        this.content = content;
    }

    public Tag() {

    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", content='" + content + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

//    public void addPost(Post post) {
//        if(this.posts==null) {
//            this.posts = new ArrayList<Post>();
//        }
//        this.posts.add(post);
//    }
}
