package com.blog.demo.tag;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag,Long> {
    @Query("SELECT t FROM Tag t WHERE t.content=?1")
    List<Tag> findAllTagsByContent(String content);

//    @Query("SELECT t FROM Tag t WHERE t.content LIKE='?1_'")
//    List<Tag> findFuzzyAllTagsByContent(String prefix);
        List<Tag> findByContentStartsWith(String rating);
}
