package com.blog.demo.tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class TagService {
    private final TagRepository tagRepository;

    @Autowired
    TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }
    Tag addTag(Map<String, String> body) {
        List<Tag> tags = this.tagRepository.findAllTagsByContent(body.get("content"));
        if(tags.size()==0) {
            Tag tag = new Tag();
            tag.setContent(body.get("content"));
            this.tagRepository.save(tag);
            return tag;
        };
        throw new RuntimeException("Tag already exists");
    }

    public List<Tag> getTags(String prefix) {
        return this.tagRepository.findByContentStartsWith(prefix);
    }
}
