package com.blog.demo.user;

import java.util.UUID;

public class LoggedUser {
    public String email;
    public boolean isAdmin;
    public boolean isLogged;

    public UUID uuid;

    public LoggedUser(User user) {
        email = user.getEmail();
        isAdmin = user.isAdmin();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isLogged() {
        return isLogged;
    }

    public void setLogged(boolean logged) {
        isLogged = logged;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID newUuid) {
        uuid = newUuid;
    }

}
