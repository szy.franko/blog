package com.blog.demo.user.dto;

import com.blog.demo.user.User;

import java.util.List;
import java.util.stream.Collectors;

public class ShortUserDtoMapper {

    public static List<ShortUserDto> mapUsersToShortUserDto(
            List<User> users
    ) {
        return users.stream().map(user->mapToShortUserDto(user))
                .collect(Collectors.toList());
    }
    public static ShortUserDto mapToShortUserDto(User user) {
        return new ShortUserDto(user);
    }
}
