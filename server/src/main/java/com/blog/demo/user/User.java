package com.blog.demo.user;

import com.blog.demo.comment.Comment;
import com.blog.demo.message.Message;
import com.blog.demo.post.Post;
import jakarta.persistence.*;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import java.util.List;

@Entity
@Table
public class User {
    @Id
    @SequenceGenerator(
            name="user_sequence",
            sequenceName = "user_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "user_sequence"
    )

    private Long id;
    private static Argon2PasswordEncoder encoder = new Argon2PasswordEncoder(32,64,1,15*1024,2);


    private String email;
    private String hash;
    private boolean isAdmin;
    private boolean isLogged;
    @OneToMany
    @JoinColumn(name = "userId")
    private List<Post> posts;

    @OneToMany
    @JoinColumn(name="userId")
    private List<Comment> comments;

    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name="userIdFrom")
    private List<Message> messagesFrom;
    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name="userIdTo")
    private List<Message> messagesTo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String email, String password) {

        this.hash = User.encoder.encode(email+password);
    }

    public static boolean checkCreditentials(String email, String password, String hash) {
        return User.encoder.matches(email+password, hash);
    }
    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isLogged() {
        return isLogged;
    };

    public void setLogged(boolean isLogged) {
        this.isLogged = isLogged;
    }
    public List<Post> getPosts() {
        return this.posts;
    };

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
    public User(){
        this.isLogged = false;
    }
    public User(String email, String password, boolean isAdmin) {
        this.email = email;
        this.setHash(email,password);
        this.isAdmin = isAdmin;
        this.isLogged = false;
    }

    public List<Message> getMessagesFrom() {
        return this.messagesFrom;
    }
    public void setMessagesFrom(List<Message> messagesFrom) {
        this.messagesFrom = messagesFrom;
    }
    public List<Message> getMessagesTo() {
        return this.messagesTo;
    }
    public void setMessagesTo(List<Message> messagesTo) {
        this.messagesTo = messagesTo;
    }

    public void addMessageFrom(Message message) {
        this.messagesFrom.add(message);
    }
    public void addMessageTo(Message message) {
        this.messagesTo.add(message);
    }




}
