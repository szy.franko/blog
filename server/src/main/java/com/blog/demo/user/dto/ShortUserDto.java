package com.blog.demo.user.dto;

import com.blog.demo.post.dto.ShortPostDto;
import com.blog.demo.post.dto.ShortPostDtoMapper;
import com.blog.demo.user.User;

import java.util.List;

public class ShortUserDto {
    private Long id;
    private String email;
    private boolean isAdmin;
    private List<ShortPostDto> shortPostDtos;

    public ShortUserDto(Long id, String email, List<ShortPostDto> shortPostDtos) {
        this.id = id;
        this.email = email;
        this.shortPostDtos = shortPostDtos;
    }

    public ShortUserDto(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.isAdmin = user.isAdmin();
        this.shortPostDtos = ShortPostDtoMapper.mapPostToShortPostDto(
                user.getPosts()
        );
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }
    public boolean isAdmin() {
        return this.isAdmin;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public List<ShortPostDto> getShortPostDtos() {
        return shortPostDtos;
    }

    public void setShortPostDtos(List<ShortPostDto> shortPostDtos) {
        this.shortPostDtos = shortPostDtos;
    }
}
