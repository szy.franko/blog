package com.blog.demo.user;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Users {
    private static HashMap<UUID,LoggedUser> users;
    public static HashMap<UUID, LoggedUser> getUsers(){
        if(Users.users==null) {
            Users.users = new HashMap<UUID,LoggedUser>();
        }
        return Users.users;
    }
    public static UUID addUser(LoggedUser user) {
        if(Users.users==null) {
            Users.users = new HashMap<UUID,LoggedUser>();
        }
        UUID uuid = UUID.randomUUID();
        Users.users.put(uuid, user);
        return uuid;
    }
    public static String removeUser(String uuid) {
        Users.users.remove(UUID.fromString(uuid));
        return uuid;
    }

    public static String getUUIDByEmail(String email) {
        for(Map.Entry<UUID,LoggedUser> user:
            Users.users.entrySet()
        ) {
            if(user.getValue().getEmail().equals(email)) {
                    return user.getKey().toString();
            }
        }
        return null;
    }


}
