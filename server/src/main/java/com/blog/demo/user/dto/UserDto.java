package com.blog.demo.user.dto;


import com.blog.demo.post.dto.PostDto;

import java.util.List;

public class UserDto {

    private Long id;

    private String email;
    private boolean isAdmin;
    private boolean isLogged;
    private List<PostDto> postsDtos;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<PostDto> getPostsDtos() {
        return postsDtos;
    }
    public void setPostsDtos(List<PostDto> postsDtos) {
        this.postsDtos = postsDtos;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isLogged() {
        return isLogged;
    };

    public void setLogged(boolean isLogged) {
        this.isLogged = isLogged;
    }
    public List<PostDto> getPosts() {
        return this.postsDtos;
    };

    public void setPosts(List<PostDto> postsDtos) {
        this.postsDtos = postsDtos;
    }
    public UserDto(){
        this.isLogged = false;
    }
    public UserDto(String email, boolean isAdmin) {
        this.email = email;
        this.isAdmin = isAdmin;
        this.isLogged = false;
    }



}
