package com.blog.demo.user;

import com.blog.demo.user.dto.ShortUserDto;
import com.blog.demo.user.dto.ShortUserDtoMapper;
import com.blog.demo.user.dto.UserDtoMapper;
import com.blog.demo.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.awt.print.Pageable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.HashMap;
import java.util.UUID;
@Service
public class UserService {
    private final int PAGE_SIZE = 20;
    private final UserRepository userRepository;
    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDto> getAllUsers() {
        return UserDtoMapper.mapUserToDto(userRepository.findAll());
    }


    public List<User> getPosts(int page) {
        return userRepository.findAllUsers(
                (Pageable) PageRequest.of(page, PAGE_SIZE)
        );
    }

    public HashMap<UUID, LoggedUser> getLoggedUsers() {
        return Users.getUsers();
    }
    public void registerNewUser(String email, String password) {
        Optional<User> existingUser = userRepository.findByEmail(email);
        if(existingUser.isPresent()) {
            throw  new IllegalStateException("User with a given email already exists");
        }
        User user = new User(email,password,false);
        userRepository.save(user);

    }

    public LoggedUser loginUser(String email, String password) {
        Optional<User> userOptional = userRepository.findByEmail(email);


        if(!userOptional.isPresent()) {
            throw new IllegalStateException("User with email " + email + " does not exits");
        } else {
            userOptional.get().setLogged(true);
            if(!userOptional.get().checkCreditentials(email,password, userOptional.get().getHash())) {
                throw new IllegalStateException("Wrong creditentials");
            }
            LoggedUser loggedUser = new LoggedUser(userOptional.get());
            UUID uuid = Users.addUser(loggedUser);
            loggedUser.setUuid(uuid);
            System.out.println(Users.getUsers());
            this.userRepository.save(userOptional.get());
            return loggedUser;
        }








    }


}
