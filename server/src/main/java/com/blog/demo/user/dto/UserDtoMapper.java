package com.blog.demo.user.dto;

import com.blog.demo.post.dto.PostDtoMapper;
import com.blog.demo.user.User;

import java.util.List;
import java.util.stream.Collectors;

public class UserDtoMapper {
    UserDtoMapper() {}
    public static List<UserDto> mapUserToDto(List<User> users) {
        return users.stream().map(user->mapToUserDto(user))
                .collect(Collectors.toList());
    }

    public static UserDto mapToUserDto(User user) {
        UserDto userDto = new UserDto(
                user.getEmail(), user.isAdmin()
        );
        userDto.setPostsDtos(PostDtoMapper.mapPostToDto(user.getPosts()));
        return userDto;
    }
}
