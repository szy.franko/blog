package com.blog.demo.user;


import com.blog.demo.user.dto.UserDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.*;

import static com.blog.demo.helpers.Image.scaleAndSaveImage;

@CrossOrigin(
        origins = {
                "http://localhost:5173"
        },
        methods = {
                RequestMethod.OPTIONS,
                RequestMethod.GET,
                RequestMethod.PUT,
                RequestMethod.DELETE,
                RequestMethod.POST
        })
@RestController
public class UserController {
    private final UserService userService;
    private static final String UPLOAD_DIR = "../app/public/uploads";
    private static final String UPLOAD_DIR_MIN = "../app/public/uploads/min/";
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path="/api/v1/user")
    public List<UserDto> getUsers() {
        return userService.getAllUsers();
    }



    @GetMapping(path="/api/v1/user/logged")
    public HashMap<UUID,LoggedUser> getLoggedUsers() {
        return userService.getLoggedUsers();
    }
    @PostMapping(path = "api/v1/user")
    public HashMap<String, String> registerNewUser(@RequestBody Map<String,String > userData) {
        System.out.println(userData);
        userService.registerNewUser(userData.get("email"), userData.get("password"));
        HashMap<String, String> response = new HashMap<>();
        response.put("status", "OK");
        return response;
    }
    @PostMapping(path = "api/v1/user/login")
    public LoggedUser loginUser(@RequestBody Map<String, String> request) {
        System.out.println(request);
        return userService.loginUser(request.get("email"), request.get("password"));

    }
    @PostMapping(path = "api/v1/user/logout")
    public LoggedUser logoutUser(@RequestBody Map<String,String> request) {
        LoggedUser loggedUser =
                Users.getUsers().get(request.get("uuid"));
        Users.removeUser(request.get("uuid"));
        return loggedUser;
    }

    @PostMapping("/api/v1/user/upload")
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {
        // Sprawdzenie, czy plik nie jest pusty
        if (file.isEmpty()) {
            return ResponseEntity.badRequest().body("Przesłany plik jest pusty");
        }
        System.out.println(file.getOriginalFilename());
        System.out.println(Users.getUsers());
        LoggedUser loggedUser = Users.getUsers().get(UUID.fromString(file.getOriginalFilename()));

        try {
            scaleAndSaveImage(file,40,40,UPLOAD_DIR_MIN+loggedUser.getEmail());
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Wystąpił błąd podczas skalowania i zapisu pliku na serwerze");
        }

        try {
            // Utworzenie katalogu na serwerze, jeśli nie istnieje
            Path uploadPath = Paths.get(UPLOAD_DIR).toAbsolutePath().normalize();
            Files.createDirectories(uploadPath);

            // Pobranie nazwy pliku
            String fileName = StringUtils.cleanPath(loggedUser.email);

            // Zapis pliku na serwerze
            Path filePath = uploadPath.resolve(fileName);
            if (Files.exists(filePath)) {
                // Jeśli plik istnieje, usuń istniejący plik przed zapisaniem nowego
                Files.delete(filePath);
            }
            Files.copy(file.getInputStream(), filePath);

            // Zwrócenie odpowiedzi
            return ResponseEntity.ok("Plik został przesłany i zapisany na serwerze: " + fileName);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Wystąpił błąd podczas zapisu pliku na serwerze");
        }
    }
}

