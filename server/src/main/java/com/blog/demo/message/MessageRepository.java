package com.blog.demo.message;

import com.blog.demo.user.User;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
//    @Query("SELECT m FROM Message m INNER JOIN User u ON m.userIdFrom=m.id WHERE u.email=?1 UNION SELECT m FROM Message m INNER JOIN User u ON m.userIdTo=m.id WHERE u.email=?1")
    @Query(value = "SELECT m.content, m.created, u.email, \"FROM\" FROM message m INNER JOIN user u" +
            " ON m.user_id_from=u.id" +
            " INNER JOIN user i ON m.user_id_to=i.id WHERE i.email=:email"+
            " UNION"+
            " SELECT m.content, m.created, u.email, \"TO\" FROM message m INNER JOIN user u" +
            " ON m.user_id_to=u.id" +
            " INNER JOIN user i ON m.user_id_from=i.id WHERE i.email=:email" +
            " ORDER BY created"
            ,nativeQuery = true)
    List findMessagesByEmail(String email);

    @Query(value = "SELECT m.content, u.email FROM message m INNER JOIN user u" +
            "ON m.user_id_from=u.id" +
            "INNER JOIN user i ON m.user_id_to=i.id WHERE i.id=?1"+
            "UNION "+
            "SELECT m.content, u.email FROM message m INNER JOIN user u " +
            "ON m.userIdTo=u.id" +
            "INNER JOIN user i ON m.user_id_from=i.id WHERE i.id=?1",nativeQuery = true)
    List findMessagesByUserId(Long id);
    @Query("SELECT u FROM User u INNER JOIN Message m ON m.userIdFrom=u.id WHERE m.userIdTo=?1 UNION SELECT i FROM User i INNER JOIN Message n ON n.userIdTo=i.id WHERE n.userIdFrom=?1")
    List<User> findFriends(Long id);

//    @Query("SELECT m FROM Message INNER JOIN User u ON" +
//            " m.userIdFrom = u.id " +
//            "INNER JOIN User i " +
//            "ON m.userIdTo = i.id" +
//            " WHERE (u.email=?1 AND i.email=?2) OR " +
//            "u.email=$2 AND i.email=$1")
//    List<Message> findMessagesByEmails(Long email0, Long email1);

    @Modifying
    @Transactional
    @Query("UPDATE Message m SET m.readed=true WHERE m.userIdFrom=?1 AND m.userIdTo=?2")
    int updateReadMessagesFromTo(Long idFrom, Long idTo);

    @Query(value = "SELECT m FROM Message " +
            "INNER JOIN User u ON m.user_id_to = u.id" +
            " INNER JOIN User i ON m.user_id_from = i.id" +
            " WHERE u.id=?1 AND i.id=?2", nativeQuery = true)
    List<Message> findMessagesToByIds(Long idTo, Long idFrom);
}
