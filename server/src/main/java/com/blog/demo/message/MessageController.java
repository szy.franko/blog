package com.blog.demo.message;

import com.blog.demo.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(
        origins = {
//                "http://localhost:5173"
                "*"
        },
        methods = {
                RequestMethod.OPTIONS,
                RequestMethod.GET,
                RequestMethod.PUT,
                RequestMethod.DELETE,
                RequestMethod.POST
        })
@RestController
public class MessageController {


    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping(path = "api/v1/message/")
    public Message addMessage(@RequestBody Map<String,String> message) {
        return this.messageService.addMessage(message);
    }

    @PostMapping(path = "api/v1/message/get")
    public List getMessages(@RequestBody Map<String,String > reqObj){
        return this.messageService.getMessages(reqObj.get("uuid"));
    }

    @PostMapping(path = "api/v1/friend")
    public List getFriends(@RequestBody Map<String,String> userData) {
        return this.messageService.getFriends(userData);
    }

    @PutMapping(path = "api/v1/message/setReaded")
    public boolean setReaded(@RequestBody Map<String,String> reqObj) {
        return this.messageService.setReaded(reqObj);
    }

    @PostMapping("api/v1/message/getUnreaded")
    public List<Message> getUnreaded(
            @RequestBody Map<String,String> reqObj) {
        return this.messageService.getUnreaded(reqObj);
    }

}
