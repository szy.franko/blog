package com.blog.demo.message;

import com.blog.demo.user.LoggedUser;
import com.blog.demo.user.User;
import com.blog.demo.user.UserRepository;
import com.blog.demo.user.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class MessageService {

    private final MessageRepository messageRepository;
    private final UserRepository userRepository;
    @Autowired
    public MessageService(MessageRepository messageRepository, UserRepository userRepository) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }
    public Message addMessage(Map<String, String> message) {
        LoggedUser loggedUser = Users.getUsers().get(message.get("uuid"));
        Optional<User> userFrom = userRepository.findByEmail(loggedUser.getEmail());
        if(userFrom.isEmpty()) {
            throw new RuntimeException("User with a given email does not exist");
        }
        Optional<User> userTo =
                userRepository.findByEmail(message.get("receiver"));

        if(userTo.isEmpty()) {
            throw new RuntimeException("Receiver with a given email does not exist");
        }
        Message message1 = new Message();
        message1.setContent(message.get("content"));
        message1.setCreated(LocalDateTime.now());
        this.messageRepository.save(message1);
        userFrom.get().addMessageFrom(message1);
        userTo.get().addMessageTo(message1);
        this.userRepository.save(userFrom.get());
        this.userRepository.save(userTo.get());
        return message1;
    }
    public List getMessages(String uuid) {
        LoggedUser loggedUser = Users.getUsers().get(UUID.fromString(uuid));
        Optional<User> user = this.userRepository.findByEmail(loggedUser.getEmail());
        if(user.isEmpty()) {
            throw new RuntimeException("User with a given uuid does not exist");
        }
        List messages = this.messageRepository.findMessagesByEmail(user.get().getEmail());
//        List messages = this.messageRepository.findMessagesByUserId(user.get().getId());
        System.out.println(messages);
        return messages;
    }
    public List getFriends(Map<String, String> userData) {
        LoggedUser loggedUser = Users.getUsers().get(UUID.fromString(userData.get("uuid")));
        // TO DO
        Optional<User> user = this.userRepository.findByEmail(loggedUser.getEmail());
        if(user.isEmpty()) {
            throw new RuntimeException("User with given uuid does not exist");
        }
        List<User> friends = this.messageRepository.findFriends(user.get().getId());
        System.out.println(friends);

        return friends.stream().map(friend->friend.getEmail()).collect(Collectors.toList());



    }


    public boolean setReaded(Map<String,String> reqObj) {
        String uuid = reqObj.get("uuid");
        String emailFriend = reqObj.get("email");
        LoggedUser loggedUser = Users.getUsers()
                .get(UUID.fromString(uuid));
        Optional<User> user = this.userRepository.findByEmail(loggedUser.getEmail());
        if(user.isEmpty()) {
            throw new RuntimeException("User with a given email does not exist");
        }

        Optional<User> friend = this.userRepository.findByEmail(emailFriend);
        if(friend.isEmpty()) {
            throw new RuntimeException("User with a given email does not exist");
        }
        this.messageRepository.updateReadMessagesFromTo(friend.get().getId(),user.get().getId());
        return true;

    }

    public List<Message> getUnreaded(Map<String, String> reqObj) {
        Optional<User> friend = this.userRepository.findByEmail(reqObj.get("email"));
        if(friend.isEmpty()) {
            throw new RuntimeException("User with a given email does not exist");
        }
        LoggedUser loggedUser = Users.getUsers().get(reqObj.get("uuid"));
        Optional<User> user = this.userRepository.findByEmail(loggedUser.getEmail());
        return this.messageRepository.findMessagesToByIds(user.get().getId(), friend.get().getId());

    }
}
