package com.blog.demo.comment;


import jakarta.persistence.*;

import java.util.List;


@Entity
public class Comment {
    @Id
    @SequenceGenerator(
            name="comment_sequence",
            sequenceName = "comment_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "comment_sequence"
    )
    private Long id;
    @Column(columnDefinition="LONGTEXT")
    private String content;

    private String userName;
    private Long UserId;
    @Column(nullable = true)
    private Long PostId;
    @Column(nullable = true)
    private Long CommentId;

    @OneToMany
    @JoinColumn(name="commentId")
    private List<Comment> comments;

    public Comment() {};
    public Comment(String content, Long userId, Long postId, Long commentId) {
        this.content = content;
        UserId = userId;
        PostId = postId;
        CommentId = commentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserName() {
        return this.userName;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public Long getUserId() {
        return UserId;
    }

    public void setUserId(Long userId) {
        UserId = userId;
    }

    public Long getPostId() {
        return PostId;
    }

    public void setPostId(Long postId) {
        PostId = postId;
    }

    public Long getCommentId() {
        return CommentId;
    }

    public void setCommentId(Long commentId) {
        CommentId = commentId;
    }
}
