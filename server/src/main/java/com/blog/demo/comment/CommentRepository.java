package com.blog.demo.comment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment,Long> {
//    @Query("SELECT c FROM Comment c WHERE c.comment_id=?1")
//    List<Comment> find
}
