package com.blog.demo.comment;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.List;
@CrossOrigin(
        origins = {
//                "http://localhost:5173"
                "*"
        },
        methods = {
                RequestMethod.OPTIONS,
                RequestMethod.GET,
                RequestMethod.PUT,
                RequestMethod.DELETE,
                RequestMethod.POST
        })
@RestController
public class CommentController {
    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService){
        this.commentService = commentService;
    }

    @GetMapping(path = "api/v1/comment/post/{id}")
    public List<Comment> getCommentsByPost(
            @PathVariable Long id){
        return this.commentService.getCommentsByPost(id);

    }
    @PostMapping(path = "api/v1/comment/post/{id}")
    public Comment addComment(
            @PathVariable Long id,
            @RequestBody Map<String,String> commentData) {
        return this.commentService.addComment(id,commentData);
    }

    @GetMapping(path = "api/v1/comment/comment/{id}")
    public List<Comment> getCommentsByComments(@PathVariable Long id) {
        return this.commentService.getCommentsByComment(id);
    }
    @PostMapping(path = "api/v1/comment/comment/{id}")
    public Comment  addCommentToComment(@PathVariable Long id,
                                        @RequestBody Map<String, String> commentData) {
        return this.commentService.addCommentToComment(id,commentData);
    }

}

