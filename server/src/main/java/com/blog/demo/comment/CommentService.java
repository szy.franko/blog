package com.blog.demo.comment;

import com.blog.demo.post.Post;
import com.blog.demo.post.PostRepository;
import com.blog.demo.user.LoggedUser;
import com.blog.demo.user.UserRepository;
import com.blog.demo.user.Users;
import com.blog.demo.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CommentService {
    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    @Autowired
    public CommentService(
            CommentRepository commentRepository,
            UserRepository userRepository,
            PostRepository postRepository
    ) {
       this.commentRepository = commentRepository;
       this.userRepository = userRepository;
       this.postRepository = postRepository;
    }

    public List<Comment> getCommentsByPost(Long id) {
        Optional<Post> post = this.postRepository.findById(id);
        if(post.isEmpty()) {
            throw new RuntimeException("Post with a given id does not exist");
        }
        for (Comment comment: post.get().getComments()) {
            if(comment.getUserName()==null) {
                comment.setUserName(this.userRepository.findById(comment.getUserId()).get().getEmail());
            }
        }
        return post.get().getComments();
    }
    public Comment addComment(Long id, Map<String, String> commentData) {

        LoggedUser loggedUser = Users.getUsers()
                .get(UUID.fromString(commentData.get("uuid")));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.now();
        Optional<User> user = this.userRepository
                .findByEmail(loggedUser.getEmail());
        if(user.isEmpty()) {
            throw new RuntimeException("Given email is not correct");
        }
        Comment comment = new Comment(commentData.get("content"),
                user.get().getId(),id, null
                );
        this.commentRepository.save(comment);
        return comment;
    };

    public List<Comment> getCommentsByComment(Long id) {
        Optional<Comment> comment = this.commentRepository.findById(id);
        if(comment.isEmpty()) {
            throw new RuntimeException("Comment with a given id does not exist");
        }
        return comment.get().getComments();
    }
    public Comment addCommentToComment(Long id, Map<String,String> commentData) {
        LoggedUser loggedUser = Users.getUsers()
                .get(UUID.fromString(commentData.get("uuid")));
        String email = loggedUser.getEmail();
        Optional<User> user =this.userRepository.findByEmail(email);
        if(user.isEmpty()) {
            throw new RuntimeException("User with a given email does not exist");
        }

        Comment comment = new Comment(commentData.get("content"),
                user.get().getId(),null,id);
        comment.setUserName(user.get().getEmail());
        this.commentRepository.save(comment);
        return comment;
    }
}
