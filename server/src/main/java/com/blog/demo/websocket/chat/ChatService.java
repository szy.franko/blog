package com.blog.demo.websocket.chat;

import com.blog.demo.message.Message;
import com.blog.demo.message.MessageRepository;
import com.blog.demo.user.LoggedUser;
import com.blog.demo.user.User;
import com.blog.demo.user.UserRepository;
import com.blog.demo.user.Users;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class ChatService {

    private final MessageRepository messageRepository;
    private final UserRepository userRepository;
    public ChatService(MessageRepository messageRepository,
                       UserRepository userRepository) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }
    public void sendMessage(ChatMessage chatMessage, SimpMessagingTemplate simpMessagingTemplate) {
        System.out.println(chatMessage.getType());
        System.out.println(chatMessage.getSender());
        System.out.println(chatMessage.getContent());
        MessageType type = chatMessage.getType();
        String sender = chatMessage.getSender();
        String receiver = chatMessage.getReceiver();
        String content = chatMessage.getContent();
        LoggedUser loggedUser = Users.getUsers().get(UUID.fromString(sender));
        Optional<User> userFrom = userRepository.findByEmail(loggedUser.getEmail());
        if(userFrom.isEmpty()) {
            throw new RuntimeException("User with a given email does not exist");
        }
        Optional<User> userTo =
                userRepository.findByEmail(receiver);

        if(userTo.isEmpty()) {
            throw new RuntimeException("Receiver with a given email does not exist");
        }

        Message message1 = new Message();
        message1.setContent(content);
        message1.setUserIdFrom(userFrom.get().getId());
        message1.setUserIdTo(userTo.get().getId());
        message1.setReaded(false);
        LocalDateTime created =  LocalDateTime.now();
        message1.setCreated(created);
        this.messageRepository.save(message1);
        userFrom.get().addMessageFrom(message1);
        userTo.get().addMessageTo(message1);
        this.userRepository.save(userFrom.get());
        this.userRepository.save(userTo.get());
        String uuid = Users.getUUIDByEmail(receiver);
        System.out.println("uuid");
        System.out.println(uuid);
        simpMessagingTemplate.convertAndSend(
                "/topic/"+uuid,
                String.format("{\"sender\": \"%s\", \"content\": \"%s\", \"type\": \"CHAT\", \"created\":\"%s\"}",
                        userFrom.get().getEmail(),content.replace("\"","\\\""), created));
    }
}
