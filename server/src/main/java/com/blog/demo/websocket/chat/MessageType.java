package com.blog.demo.websocket.chat;

public enum MessageType {

    CHAT,
    JOIN,
    LEAVE
}
