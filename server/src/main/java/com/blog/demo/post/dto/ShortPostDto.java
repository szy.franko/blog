package com.blog.demo.post.dto;

import com.blog.demo.post.Post;

import java.time.LocalDateTime;

public class ShortPostDto {
    private Long id;
    private String title;
    private LocalDateTime created;
    private Long userId;

    public ShortPostDto(Long id, String title, LocalDateTime created, Long userId) {
        this.id = id;
        this.title = title;
        this.created = created;
        this.userId = userId;
    }

    public ShortPostDto(Post post) {
        this.id=post.getId();
        this.title = post.getTitle();
        this.created = post.getCreated();
        this.userId = post.getUserId();
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
