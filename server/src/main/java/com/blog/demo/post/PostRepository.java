package com.blog.demo.post;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.xml.crypto.Data;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post,Long> {
    @Query("Select p From Post p")
    List<Post> findAllPosts(Pageable page);
    @Query("Select Min(created) from Post")
    LocalDateTime getMinPost();
    @Query(value = "SELECT post.id, created, title, content,email from post INNER JOIN user ON post.user_id=user.id " +
            "WHERE created >=?1 AND created<=?2", nativeQuery = true)
    List<Object> findAllPostsBetween(LocalDateTime dateLower, LocalDateTime dateUpper);
}
