package com.blog.demo.post;

import com.blog.demo.user.LoggedUser;
import com.blog.demo.post.dto.PostDto;
import com.blog.demo.user.User;
import com.blog.demo.user.UserRepository;
import com.blog.demo.user.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class PostService {
    private static final int PAGE_SIZE = 20;
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    @Autowired
    public PostService(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }
    public Post addPost(Long idUser,  Map<String,String> postData) {
        System.out.println(postData);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(postData.get("created"), formatter);
        Post post = new Post(postData.get("title"), postData.get("content"), dateTime, idUser);
        this.postRepository.save(post);
        return post;
    }
    public Post createPost(Map<String, String> postData) {
        System.out.println(Users.getUsers());
        System.out.println(postData.get("uuid"));
        LoggedUser loggedUser = Users.getUsers()
                .get(UUID.fromString(postData.get("uuid")));
        if(!loggedUser.isAdmin) {
            throw new RuntimeException("User is not admin");
        }

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        LocalDateTime dateTime = LocalDateTime.now();
            Optional<User> user = this.userRepository.findByEmail(loggedUser.getEmail());
            if(user.isEmpty()) {
                throw new RuntimeException("Given email is not correct");
            }

            Post post = new Post(postData.get("title"), postData.get("content"), dateTime, user.get().getId());
            this.postRepository.save(post);
            return post;



    }
    public List<Post> getPosts(int page) {
        return postRepository.findAllPosts(
                PageRequest.of(page, PAGE_SIZE)
        );
    }
    public PostDto getPostById(Long id) {
        Optional<Post> post = postRepository.findById(id);
        if(post.isEmpty()) {
            throw new RuntimeException("Post with a given id does not exist");
        }
        return new PostDto(post.get());
    }

    public boolean positivePost(Long id, String uuid) {
        Optional<Post> post = postRepository.findById(id);
        if(post.isEmpty()) {
            throw new RuntimeException("Post with a given id does not exit");
        }
        String email = Users.getUsers()
                .get(UUID.fromString(uuid)).getEmail();
        Optional<User> user = userRepository.findByEmail(email);
        if(user.isEmpty()) {
            throw new RuntimeException("User does not exist");
        }
        post.get().addPositive(user.get());
        postRepository.save(post.get());
        return true;
    }

    public boolean negativePost(Long id, String uuid) {
        Optional<Post> post = postRepository.findById(id);
        if(post.isEmpty()) {
            throw new RuntimeException("Post with a given id does not exit");
        }
        String email = Users.getUsers()
                .get(UUID.fromString(uuid)).getEmail();
        Optional<User> user = userRepository.findByEmail(email);
        if(user.isEmpty()) {
            throw new RuntimeException("User does not exist");
        }
        post.get().addNegative(user.get());
        postRepository.save(post.get());
        return true;
    }

    public int getPostsPositive(Long id) {
        Optional<Post> post = this.postRepository.findById(id);
        if(post.isEmpty()) {
            throw new RuntimeException("Post wit a given id does not exist");
        }
        return post.get().getPositive().size();
    }

    public int getPostsNegative(Long id) {
        Optional<Post> post = this.postRepository.findById(id);
        if(post.isEmpty()) {
            throw new RuntimeException("Post wit a given id does not exist");
        }
        return post.get().getNegative().size();
    }

    public String getPostsUserRating(Long idPost, String uuid) {
        String email = Users.getUsers()
                .get(UUID.fromString(uuid)).getEmail();

        Optional<User> user = userRepository.findByEmail(email);
        if(user.isEmpty()) {
            throw new RuntimeException("User with a given UUID does mot exist");
        }

        Optional<Post> post = postRepository.findById(idPost);
        if(post.isEmpty()) {
            throw new RuntimeException("Post with a given id does not exist");
        }
        boolean isPositive =
                post.get().getPositive()
                        .stream().anyMatch(user0->user0.getEmail()==user.get().getEmail());
        if(isPositive) { return "positive"; };

        boolean isNegative =
                post.get().getNegative()
                        .stream().anyMatch(user0->user0.getEmail()==user.get().getEmail());
        if(isNegative) { return "negative"; };

        return "neutral";

    }

    public LocalDateTime getMinPost() {
        return this.postRepository.getMinPost();
    }

    public List<Object> getAllPostsInMonth(Integer year, Integer month) {
        String dateLower = year + "-" + (month < 10 ? "0" + month : month) + "-01";

        // Get the last day of the month
        int lastDay = YearMonth.of(year, month).lengthOfMonth();

        String dateUpper = year + "-" + (month < 10 ? "0" + month : month) + "-" + lastDay;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        LocalDateTime localDateTimeLower = LocalDateTime.parse(dateLower + "T00:00:00", formatter);

        // Add the time component "23:59:59" to the upper bound
        LocalDateTime localDateTimeUpper = LocalDateTime.parse(dateUpper + "T23:59:59", formatter);

        return postRepository.findAllPostsBetween(localDateTimeLower, localDateTimeUpper);
    }
}
