package com.blog.demo.post;

import com.blog.demo.post.dto.PostDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

@CrossOrigin(
        origins = {
//                "http://localhost:5173"
                "*"
        },
        methods = {
                RequestMethod.OPTIONS,
                RequestMethod.GET,
                RequestMethod.PUT,
                RequestMethod.DELETE,
                RequestMethod.POST
        })

@RestController
public class PostController {
    private final PostService postService;
    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping(path = "api/v1/post/user/{id}")
    public Post addPost(@PathVariable Long id, @RequestBody Map<String,String> postData) {
        return this.postService.addPost(id,postData);
    }

    @PostMapping(path = "api/v1/post")
    public Post createPost(@RequestBody Map<String,String> postData) {
        return this.postService.createPost(postData);
    }

    @GetMapping(path="/api/v1/post")
    @ResponseBody
    public List<Object> getShortUsers(
            @RequestParam Integer year,
            @RequestParam Integer month
    ) {
        System.out.println(year);
        System.out.println(month);
        return postService.getAllPostsInMonth(year, month);
    }
    @GetMapping("/api/v1/post/{id}")
    public PostDto getPostById(@PathVariable Long id) {
        return postService.getPostById(id);
    }
    @PutMapping("/api/v1/post/positive/{id}")
    public boolean positivePost(@PathVariable Long id, @RequestBody Map<String,String> data) {
        return postService.positivePost(id, data.get("uuid") );
    }

    @PutMapping("/api/v1/post/negative/{id}")
    public boolean negativePost(@PathVariable Long id, @RequestBody Map<String,String> data) {
        return postService.negativePost(id, data.get("uuid") );
    }

    @GetMapping("/api/v1/post/positive/{id}")
    public int getPostsPositive(@PathVariable Long id) {
        return postService.getPostsPositive(id);
    }

    @GetMapping("/api/v1/post/negative/{id}")
    public int getPostsNegative(@PathVariable Long id) {
        return postService.getPostsNegative(id);
    }

    @PostMapping("/api/v1/user/post/{idPost}/")
    public String getPostsUserRating(
            @PathVariable Long idPost,
            @RequestBody Map<String,String > data
            ) {
        return postService.getPostsUserRating(idPost, data.get("uuid"));
    }

    @GetMapping("/api/v1/post/min")
    public String getMinPost() {
        LocalDateTime date = postService.getMinPost();
        if(date==null) {
            return null;
        }
        return date.toString();
    }

}
