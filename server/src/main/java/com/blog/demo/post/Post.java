package com.blog.demo.post;

import com.blog.demo.comment.Comment;
import com.blog.demo.user.User;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Entity

public class Post {
    @Id
    @SequenceGenerator(
            name="post_sequence",
            sequenceName = "post_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "post_sequence"
    )
    private Long id;
    private String title;
    @Column(columnDefinition="LONGTEXT")
    private String content;
    private LocalDateTime created;
    private Long userId;

    @OneToMany
    @JoinColumn(name="PostId")
    private List<Comment> comments;
    @ManyToMany
    @JoinColumn(name="PositiveId")
    private  List<User> positive;
    @ManyToMany
    @JoinColumn(name="NegativeId")
    private  List<User> negative;

    public Post(){};
    public Post(String title, String content, LocalDateTime created, Long userId) {
        this.title = title;
        this.content = content;
        this.created = created;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public List<Comment> getComments() {
        return this.comments;
    }
    public List<User> getPositive() {
        return this.positive;
    }

    public List<User> getNegative() {
        return this.negative;
    }

    public void addPositive(User user) {
        if(this.positive
                .stream()
                .anyMatch(user0->user0.getId()==user.getId())
        ) {
            return;
        }
        this.negative = this.negative.stream()
                .filter(user0->user0.getId()!=user.getId())
                .collect(Collectors.toList());

        this.positive.add(user);

    }

    public void addNegative(User user) {
        if(this.negative
                .stream()
                .anyMatch(user0->user0.getId()==user.getId())
        ) {
            return;
        }
        this.positive = this.positive.stream()
                .filter(user0->user0.getId()!=user.getId())
                .collect(Collectors.toList());

        this.negative.add(user);

    }

}
