package com.blog.demo.post.dto;

import com.blog.demo.post.Post;

import java.util.List;
import java.util.stream.Collectors;

public class PostDtoMapper {
    PostDtoMapper() {}
    public static List<PostDto> mapPostToDto(List<Post> posts) {
        return posts.stream().map(post->mapToPostDto(post))
                .collect(Collectors.toList());
    }

    public static PostDto mapToPostDto(Post post) {
        return new PostDto(post);
    }
}
