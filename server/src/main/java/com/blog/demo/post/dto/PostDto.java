package com.blog.demo.post.dto;

import com.blog.demo.post.Post;

import java.time.LocalDateTime;

public class PostDto {
    public PostDto(String title, String content, LocalDateTime created) {
        this.title = title;
        this.content = content;
        this.created = created;
    }
    public PostDto(Post post) {
        this.id = post.getId();
        this.title = post.getTitle();
        this.content = post.getContent();
        this.created = post.getCreated();
        this.positiveCommentsLength =
                 post.getPositive().size();
        this.negativeCommentsLength =
                post.getNegative().size();

    }

    private Long id;
    private String title;
    private String content;
    private LocalDateTime created;

    private int positiveCommentsLength;
    private int negativeCommentsLength;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public int getPositiveCommentsLength() {
        return this.positiveCommentsLength;
    }

    public int getNegativeCommentsLength() {
        return this.negativeCommentsLength;
    }
}
