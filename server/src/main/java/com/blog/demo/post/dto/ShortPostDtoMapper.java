package com.blog.demo.post.dto;

import com.blog.demo.post.Post;

import java.util.List;
import java.util.stream.Collectors;

public class ShortPostDtoMapper {
    public static List<ShortPostDto>
        mapPostToShortPostDto(List<Post> posts) {
        return posts.stream().map(post-> mapToShortDto(post))
                .collect(Collectors.toList());
    }

    public static ShortPostDto mapToShortDto(Post post) {
        return new ShortPostDto(post);
    }
}
