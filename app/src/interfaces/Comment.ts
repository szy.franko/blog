export default interface Comment {
    id: number;
    content: string;
    userName: string;
    comments: Comment[];
    userId: number;
    postId: number;
    commentId: number | null;
}