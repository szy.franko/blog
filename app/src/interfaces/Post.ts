export default interface Post {
    id: number,
    title: string,
    content: string,
    created: string,
    userId: number
}