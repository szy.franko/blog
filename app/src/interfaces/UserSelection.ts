export default interface UserSelection {
    value?: string,
    label?: string
}