export default interface ShortPostDto {
    id: number,
    title: string,
    created: string,
    userEmail: string
}