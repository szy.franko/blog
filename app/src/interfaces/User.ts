import Post from './Post';
import shortPostDto from './shortPostDto';
export default interface User {
    id: number | null
    email: string,
    posts?: Post[],
    shortPostDtos: shortPostDto[],
    logged: boolean,
    admin: boolean
}