export default interface PostDto {
    id: number,
    title: string,
    content: string,
    created: Date,
    userId: number,
    userEmail: string
}