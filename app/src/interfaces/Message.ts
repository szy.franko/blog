export default interface Message {
    sender: String
    created: Date,
    content: String
}