import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue';
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },

  {
    path: '/blog',
    name: 'Blog',
    component: () => import(/* webpackChunkName: "about" */ '../views/Blog.vue')
  },

  {
    path: '/post/:id/:author',
    name: 'Post',
    component: () => import(/* webpackChunkName: "about" */ '../views/Post.vue')
  },

  {
    path: '/communicator/:author',
    name: "CommunicatorByAuthor",
    component: () => import(/* webpackChunkName: "about" */ '../views/Communicator.vue')
  },

  {
    path: '/communicator/',
    name: 'Communicator',
    component: () => import(/* webpackChunkName: "about" */ '../views/Communicator.vue')
  },

  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import(/* webpackChunkName: "about" */ '../views/Profile.vue')
  },

  {
    path: '/register',
    name: 'Register',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Register.vue')
  }
]
const router = createRouter({
  history: createWebHistory(),
  routes
})
export default router