import { defineStore } from 'pinia';

export interface User {
  email?: string,
  isAdmin?: boolean,
  uuid?: string
}

interface State {
  count: number,
  user: User,
  stompClient: null | object,
  messages: object,
  notifyMessage(o:Message): void,
  subscriptionEstablished: boolean
}

interface Message {
  sender: string,
  created: string,
  content: string
}


export const useStore = defineStore('counter', {
  state: (): State => ({
    count: 0, user: {}, stompClient: null, messages: {},
    notifyMessage: (obj:Message)=> {}, subscriptionEstablished: false
  }),
  getters: {
    doubleCount: (state) => state.count * 2,
    getUser: state => state.user
  },
  actions: {
    increment() {
      this.count++
    },

    setUser(user: User): void {
      this.user = user;
    },

    wanishUser(): void {
      this.user = {};
    },

    setNotifyMessage(notifyMessage:(a:Message)=>void){
      this.notifyMessage = notifyMessage;
    },

    connect(): void {
      // @ts-ignore
      const socket = new SockJS('http://localhost:8080/ws');
      // @ts-ignore
      this.stompClient = Stomp.over(socket);
      this.stompClient.connect({}, this.onConnected, this.onError);
    },

    disconnect(){
      this.stompClient.disconnect();
    },

    sendMessage(content: String, receiver: String):void {
      if(content && this.stompClient) {
          const chatMessage = {
              sender: this.user.uuid,
              content,
              type: 'CHAT',
              receiver
          };
          this.stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
      }     
  },

    onConnected() {
      if(this.subscriptionEstablished) return;
      // Subscribe to the Public Topic
      this.stompClient.subscribe('/topic/'+this.user.uuid, this.onMessageReceived);
      this.subscriptionEstablished = true;
      // Tell your username to the server
      this.stompClient.send("/app/chat.addUser",
        {},
        JSON.stringify({ sender: this.user.uuid, type: 'JOIN' })
      )
    },

    onMessageReceived(payload) {
      const message = JSON.parse(payload.body); // {sender, type, content, created}

      if (message.type === 'JOIN') {
      } else if (message.type === 'LEAVE') {
      } else {
        this.notifyMessage(message);
      }

    },

    setMessages(messages) {
      this.messages = messages;
    }

  },
})





// function onError(error) {
//   connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
//   connectingElement.style.color = 'red';
// }


