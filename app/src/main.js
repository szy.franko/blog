import './assets/main.css'

import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'


import mitt from 'mitt';
import { createPinia } from 'pinia';
import router from './router'
import App from './App.vue'

const emitter = mitt();
const app = createApp(App)

app.use(ElementPlus)

app.use(router)
app.config.globalProperties.emitter = emitter;
app.use(createPinia())  
app.mount('#app')