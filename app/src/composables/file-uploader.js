export async function uploadFile(file, url, uuid,callback) {
	// set up the request data


	let newFile = new File([file.file], uuid, { type: file.type });
	let formData = new FormData()
	formData.append('file', newFile)	
	formData.append('uuid', uuid);

	// track status and upload file
	file.status = 'loading'
	let response = await fetch(url, { method: 'POST', body: formData })
	callback();

	// change status to indicate the success of the upload request
	file.status = response.ok

	return response
}

export function uploadFiles(files, url,uuid, callback) {
	return Promise.all(files.map((file) => uploadFile(file, url,uuid,callback)))
}

export default function createUploader(url, uuid) {
	return {
		uploadFile: function (file) {
			
			return uploadFile(file, url, uuid);
		},
		uploadFiles: function (files, callback) {
			return uploadFiles(files, url, uuid,callback);
		},
	}
}
