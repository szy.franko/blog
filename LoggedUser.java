package com.example.demo.user;

public class LoggedUser {
    private String email;
    private boolean isAdmin;
    private boolean isLogged;

    public LoggedUser(User user) {
        email = user.getEmail();
        isAdmin = user.isAdmin();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isLogged() {
        return isLogged;
    }

    public void setLogged(boolean logged) {
        isLogged = logged;
    }
}
