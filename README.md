﻿# Blog

This is a simple blog written in Vue3 in Java Spring Boot Framework.
It requires MySQL database engine.
To install this app navigate to app folder and run:

    npm install

 
To run this app just run in the mentioned folder: 

    npm run dev
   
and on the other side you can open server project in IntelliJ IDEA IDE
to and run DemoApplication.


